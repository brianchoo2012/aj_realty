from django.shortcuts import render
from django.views.generic import TemplateView

class LoginView(TemplateView):
    template_name = "user/login.html"

class DashboardView(TemplateView):
    template_name = "user/dashboard.html"

class PropertyListingView(TemplateView):
    template_name = "user/property-listing.html"

class PropertyCommissionView(TemplateView):
    template_name = "user/property-commission.html"

class PropertyRegistrationListView(TemplateView):
    template_name = "user/property-registration-list.html"

class PropertyDetailsView(TemplateView):
    template_name = "user/property-details.html"

class AnnouncementView(TemplateView):
    template_name = "user/announcement.html"

class ProfileView(TemplateView):
    template_name = "user/profile.html"

class EmailView(TemplateView):
    template_name = "email/email.html"

class Error404View(TemplateView):
    template_name = 'error/error404.html'

class Error500View(TemplateView):
    template_name = 'error/error500.html'
