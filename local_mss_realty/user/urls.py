from django.urls import path
from .views import LoginView, DashboardView, PropertyListingView, PropertyCommissionView, PropertyRegistrationListView, PropertyDetailsView, ProfileView, EmailView, Error404View, Error500View, AnnouncementView

app_name = 'user'

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('dashboard/', DashboardView.as_view(), name='dashboard'),
     path('announcement/', AnnouncementView.as_view(), name='announcement'),
    path('property-listing/', PropertyListingView.as_view(), name='property-listing'),
    path('property-details/', PropertyDetailsView.as_view(), name='property-details'),
    path('property-commission/', PropertyCommissionView.as_view(), name='property-commission'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('property-registration/', PropertyRegistrationListView.as_view(), name='property-registration-list'),
    path('email/', EmailView.as_view(), name='email'),
    path('error404/', Error404View.as_view(), name='error404'),
    path('error500/', Error500View.as_view(), name='error500'),
]