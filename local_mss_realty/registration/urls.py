from django.urls import path
from .views import RegistrationView, RegistrationPaymentView
# , LoginView, DashboardView, PropertyListingView, PropertyCommissionView, PropertyRegistrationListView, PropertyDetailsView, ProfileView

app_name = 'registration'

urlpatterns = [
    path('', RegistrationView.as_view(), name='registration'),
    path('payment/', RegistrationPaymentView.as_view(), name='registration-payment'),
    # path('login/', LoginView.as_view(), name='login'),
    # path('dashboard/', DashboardView.as_view(), name='dashboard'),
    # path('property-listing/', PropertyListingView.as_view(), name='property-listing'),
    # path('property-details/', PropertyDetailsView.as_view(), name='property-details'),
    # path('property-commission/', PropertyCommissionView.as_view(), name='property-commission'),
    # path('profile/', ProfileView.as_view(), name='profile'),
    # path('property-registration/', PropertyRegistrationListView.as_view(), name='property-registration-list'),
]