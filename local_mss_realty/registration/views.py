from django.shortcuts import render
from django.views.generic import TemplateView

class RegistrationView(TemplateView):
    template_name = "registration/registration.html"

class RegistrationPaymentView(TemplateView):
    template_name = "registration/registration-payment.html"

# class LoginView(TemplateView):
#     template_name = "registration/login.html"

# class DashboardView(TemplateView):
#     template_name = "registration/dashboard.html"

# class PropertyListingView(TemplateView):
#     template_name = "registration/property-listing.html"

# class PropertyCommissionView(TemplateView):
#     template_name = "registration/property-commission.html"

# class PropertyRegistrationListView(TemplateView):
#     template_name = "registration/property-registration-list.html"

# class PropertyDetailsView(TemplateView):
#     template_name = "registration/property-details.html"

# class ProfileView(TemplateView):
#     template_name = "registration/profile.html"